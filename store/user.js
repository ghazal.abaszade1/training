import BlogService from '@/services/BlogService';


export const state = () => ({
    loading: false,
    users: [],
    user:''

});

export const mutations = {
    ADD_USER(state, user) {
        state.users.push(user)
    },

    FIND_USER(state, user) {
        state.user = user;
        console.log( state.user)
    },
};

export const actions = {
    createRegister({commit}, user) {

        return BlogService.postUser(user).then(() => {
            commit('ADD_USER', user)
        })

    },

    findUser({commit}, user) {
        return BlogService.getUser(user).then(resp => {
            console.log(resp.data[0].name)
        commit('FIND_USER', resp.data[0].name)
        })
    },
};

export const getters = {

};
