import BlogService from '@/services/BlogService';


export const state = () => ({
    loading: false,
    blogs: [],
    blog: {}
});

export const mutations = {
    SET_LOADING(state, status) {
        state.loading = status;
    },
    ADD_BLOG(state, blog) {
        state.blogs.push(blog)
    },
    SET_BLOGS(state, blogs) {
        state.blogs = blogs;
    },
    SET_EVENTS_TOTAL(state, eventsTotal) {
        state.eventsTotal = eventsTotal;
    },
    SET_BLOG(state, blog) {
        state.blog = blog;
    },
};

export const actions = {
    createBlog({commit}, blog) {
        // commit('SET_LOADING', true);

        return BlogService.postBlog(blog).then(() => {
            commit('ADD_BLOG', blog)
        })

    },
    fetchBlogs({commit}, {perPage, page}) {
        BlogService.getBlogs(perPage, page)
            .then(response => {
                // commit(' SET_EVENTS_TOTAL', parseInt(response.headers['x-total-count']))
                console.log('total was are' + response.headers['x-total-count'])
                commit('SET_BLOGS', response.data)
            })
            .catch(error => {
                console.log('there was a problem', error.response)
            })
    },
    fetchBlog({commit, getters}, id) {
        var blog = getters.getBlogById(id)

        if (blog) {
            commit('SET_BLOG', blog)
        } else {
            BlogService.getBlog(id)
                .then(response => {
                    commit('SET_BLOG', response.data)
                })
                .catch(error => {
                    console.log('there was a problem: ', error.response)
                })
        }

    }

};

export const getters = {
    settings: state => {
        return {
            blog: state.blog,
            blogs: state.blogs,
        }
    },
    isLoading(state) {
        return state.loading;
    },

    getBlogById: state => id => {
        return state.blogs.find(blog => blog.id === id)
    }
};
