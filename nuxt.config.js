export default {
    head: {
        titleTemplate: '%s -ویرگول ',
        title: 'ویرگول ',
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: ''},
            {name: 'msapplication-TileColor', content: '#ffffff'},
            {name: 'msapplication-TileImage', content: 'favicon/design-icon.png'},
            {name: 'theme-color', content: '#ffffff'},
        ],
        link: [
        ]
    },

    css: ['~assets/scss/all.scss', 'swiper/swiper-bundle.css'],

    plugins: [
        {src: '@/plugins/vue-awesome-swiper', mode: 'client'},
    ],

    components: false,

    buildModules: [
        '@nuxtjs/vuetify',
        '@nuxtjs/style-resources',
    ],

    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/gtm',
        [
            'nuxt-i18n',
            {
                lazy: true,
                locales: [
                    {
                        name: 'Farsi',
                        code: 'fa',
                        iso: 'fa-IR',
                        file: 'fa-IR.js',
                    },
                ],
                langDir: 'locales/',
                defaultLocale: 'fa',
                strategy: 'no_prefix',
            },
        ],
    ],

    gtm: {
        id: process.env.GTM_ID,
        respectDoNotTrack: false
    },

    vuetify: {
        rtl: true,
        treeShake: true,
        defaultAssets: false,
        theme: {
            dark: true,
            options: {
                customProperties: true,
            },

            themes: {
                dark: {
                    primary: '#a62626',
                    secondary: '#F2F0F7',
                    'text-dark': '#313c57',
                    'text-light': '#fff',
                },
            },
        },

    },

    router: {
        base: process.env.SITE_PREFIX
    },

    build: {
        extractCSS: {
            ignoreOrder: true
        },
    },

    styleResources: {
        scss: ['~assets/scss/variables.scss'],
    },
}
