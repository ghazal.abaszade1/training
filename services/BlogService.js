import axios from 'axios'

const apiClient = axios.create({
    baseURL: 'http://localhost:3000',
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
    }
})
export default {

    getBlogs(perPage , page) {
        return apiClient.get('/events?_limit=' + perPage + '&_page=' + page)
    },

    getBlog(id) {
        return apiClient.get('/events/' + id)
    },
    postBlog(blog) {
        return apiClient.post('/events', blog)
    },
    postUser(user) {
        return apiClient.post('/login', user)
    },
    getUser(user) {
        return apiClient.get('/login?q=' + user)
    }
}
