export default i18n => ({
    emailRule(value) {
        if (value) {
            const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            return pattern.test(value) || i18n.t('rules.email');
        }

        return true;
    },

    phoneRule(value) {
        if (value) {
            const pattern = /^([0|+[0-9]{1,5})?([7-9][0-9]{9})$/;
            return pattern.test(value) || i18n.t('rules.phone');
        }

        return true;
    },

    phoneOrEmailRule(value) {
        const isPhone = this.phoneRule(value)
        const isEmail = this.phoneRule(value)

        return (isPhone === true || isEmail === true) || i18n.t('rules.phoneOrEmail');
    },


    requiredRule(value) {
        return !!value || i18n.t('rules.required');
    },

    isNumericRule(value) {
        return !isNaN(value) || i18n.t('rules.numeric');
    },

    exactRule(exact, value) {
        if (value) {
            exact = parseInt(exact);
            value = value.length;

            if (value === exact) {
                return true;
            }
        }

        return i18n.t('rules.exact', {exact: exact});
    },

    minRule(min, value) {
        min = parseInt(min);
        value = parseInt(value);

        if (min <= value) {
            return true;
        }

        return i18n.t('rules.min', {min: min});
    },

    maxRule(max, value) {
        max = parseInt(max);
        value = parseInt(value);

        if (max >= value) {
            return true;
        }

        return i18n.t('rules.max', {max: max});
    },

    minLengthRule(min, value) {
        if (value) {
            min = parseInt(min);
            value = value.length;

            if (min <= value) {
                return true;
            }

            return i18n.t('rules.minLength', {min: min});
        }

        return true;
    },

    maxLengthRule(max, value) {
        if (value) {
            max = parseInt(max);
            value = value.length;

            if (max >= value) {
                return true;
            }

            return i18n.t('rules.maxLength', {max: max});
        }

        return true;

    },

})
